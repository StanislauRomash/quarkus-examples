package org.sromash.quarkus.example;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import jakarta.inject.Inject;
import org.bson.Document;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

/**
 * Class CommandRunner.
 * <p>
 * Date: 04 17, 2024
 *
 * @author Stanislau Romash
 */
@QuarkusMain
public class CommandRunner implements QuarkusApplication {

    private static final Logger LOG = Logger.getLogger(CommandRunner.class);

    @ConfigProperty(defaultValue = "Pulp Fiction", name = "movies.search")
    private String movieToSearch;

    @Inject
    MongoClient mongoClient;


    @Override
    public int run(String... args) throws Exception {
        LOG.debug("Starting application");

        getCollection().drop();

        Movie pulpFiction = new Movie("Pulp Fiction", 1994);
        Movie fightClub = new Movie("Fight Club", 1999);

        save(pulpFiction);
        save(fightClub);

        // find all movies
        List<Movie> movies = new ArrayList<>();
        try (MongoCursor<Document> cursor = getCollection().find().iterator()) {
            while (cursor.hasNext()) {
                Document document = cursor.next();
                Movie movie = new Movie(
                        document.getString("title"),
                        document.getInteger("year")
                );
                movies.add(movie);
            }
        }

        movies.forEach(LOG::info);

        // find one movie
        FindIterable<Document> findIterable = getCollection().find(eq("title", movieToSearch));
        MongoCursor<Document> cursor = findIterable.cursor();
        Document document = cursor.next();
        cursor.close();
        Movie movie =  new Movie(
                document.getString("title"),
                document.getInteger("year")
        );
        LOG.info(movie);

        LOG.debug("Completing application");
        return 0;
    }

    private void save(Movie movie) {
        Document document = new Document()
                .append("title", movie.getTitle())
                .append("year", movie.getYear());
        getCollection().insertOne(document);
    }

    private MongoCollection<Document> getCollection() {
        return mongoClient.getDatabase("movies").getCollection("movie");
    }


}
