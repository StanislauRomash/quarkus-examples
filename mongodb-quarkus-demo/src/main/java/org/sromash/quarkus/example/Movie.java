package org.sromash.quarkus.example;

import java.util.StringJoiner;

/**
 * Class Movie.
 * <p>
 * Date: 04 17, 2024
 *
 * @author Stanislau Romash
 */
public class Movie {
    private String title;
    private Integer year;

    public Movie() {
    }

    public Movie(String title, Integer year) {
        this.title = title;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Movie.class.getSimpleName() + "[", "]")
                .add("title='" + title + "'")
                .add("year=" + year)
                .toString();
    }
}
