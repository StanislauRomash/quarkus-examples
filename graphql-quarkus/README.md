# graphql-quarkus

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## How to check

- Schema - http://localhost:8080/graphql/schema.graphql
- GraphQL UI - http://localhost:8080/q/graphql-ui/
- Sample queries:
  - find all rooms:
```
{
  allRooms {
    name
    bedInfo
    roomNumber
    __typename
  }
}
```
  - find rooms by name:
```
{
  allRooms(name:"Victoria") {
    name
    bedInfo
    roomNumber
    __typename
  }
}
```
  - find a room by number:
```
{
  room(roomNumber:"W2") {
    name
    bedInfo
    roomNumber
    __typename
  }
}
```
  - add a room:
```
mutation {
  addRoom(roomRequest: {name: "California", 
                        roomNumber: "E3", 
                        bedInfo:"4Q"}) {
    roomId
    name
    roomNumber
    bedInfo
  }
}
```
- Readiness check - http://localhost:8080/q/health/ready
- Metrics - http://localhost:8080/q/metrics
- Quarkus endpoints - http://localhost:8080/q/dev-ui/endpoints



## Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application, packaged as an _über-jar_, is now runnable using `java -jar target/*-runner.jar`.

## Creating a native executable

You can create a native executable using: 
```shell script
./mvnw package -Dnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 
```shell script
./mvnw package -Dnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/graphql-quarkus-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.

## Related Guides

- Hibernate ORM ([guide](https://quarkus.io/guides/hibernate-orm)): Define your persistent model with Hibernate ORM and Jakarta Persistence
- SmallRye GraphQL ([guide](https://quarkus.io/guides/smallrye-graphql)): Create GraphQL Endpoints using the code-first approach from MicroProfile GraphQL
- JDBC Driver - PostgreSQL ([guide](https://quarkus.io/guides/datasource)): Connect to the PostgreSQL database via JDBC
