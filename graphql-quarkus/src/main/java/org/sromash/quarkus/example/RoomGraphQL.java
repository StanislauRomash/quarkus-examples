package org.sromash.quarkus.example;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import jakarta.inject.Inject;
import org.eclipse.microprofile.graphql.Description;
import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Mutation;
import org.eclipse.microprofile.graphql.Name;
import org.eclipse.microprofile.graphql.Query;
import org.jboss.logging.Logger;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.nonNull;

/**
 * Class RoomResource.
 * <p>
 * Date: 04 22, 2024
 *
 * @author Stanislau Romash
 */
@GraphQLApi
public class RoomGraphQL {
    private static final Logger LOG = Logger.getLogger(RoomGraphQL.class);
    @Inject
    RoomService roomService;

    @Inject
    MeterRegistry meterRegistry;

    @Query("allRooms")
    @Description("Gets all rooms for the hotel")
    public List<Room> getAllRooms(@Name("name") String name) {
        Timer timer = Timer.builder("roomgraphql").tag("method", "getAllRooms").register(meterRegistry);
        long start = System.nanoTime();
        LOG.info("getAllRooms is called");

        List<Room> rooms;
        if (nonNull(name) && !name.isBlank()) {
            rooms = roomService.findByName(name);
        } else {
            rooms = roomService.findAllRooms();
        }

        long end = System.nanoTime();
        timer.record(end - start, TimeUnit.MILLISECONDS);
        return rooms;
    }

    @Query("room")
    @Description("Gets all rooms for the hotel")
    public Room getRoom(@Name("roomNumber") String roomNumber) {
        LOG.info("getRoom is called");
        return roomService.findByRoomNumber(roomNumber);
    }

    @Mutation("addRoom")
    @Description("Save one new room")
    public Room saveRoom(RoomRequest roomRequest) {
        LOG.info("saveRoom is called");
        Room room = new Room(
                roomRequest.getName(),
                roomRequest.getRoomNumber(),
                roomRequest.getBedInfo()
        );
        roomService.save(room);
        return room;
    }

}
