package org.sromash.quarkus.example;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Readiness;

/**
 * Class ReadinessProbe.
 * <p>
 * Date: 04 23, 2024
 *
 * @author Stanislau Romash
 */
@Readiness
@ApplicationScoped
public class ReadinessProbe implements HealthCheck {

    @Inject
    RoomService roomService;

    @Override
    public HealthCheckResponse call() {
        int totalRooms =  roomService.findAllRooms().size();
        return HealthCheckResponse.named("Custom Readiness Check")
                .up()
                .withData("roomCount", totalRooms)
                .build();
    }
}
