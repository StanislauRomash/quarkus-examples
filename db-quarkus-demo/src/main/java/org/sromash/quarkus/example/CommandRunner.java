package org.sromash.quarkus.example;

import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import jakarta.inject.Inject;
import org.jboss.logging.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Class CommandRunner.
 * <p>
 * Date: 04 17, 2024
 *
 * @author Stanislau Romash
 */
@QuarkusMain
public class CommandRunner implements QuarkusApplication {

    private static final Logger LOG = Logger.getLogger(CommandRunner.class);

    @Inject
    DataSource dataSource;

    @Override
    public int run(String... args) throws Exception {
        LOG.debug("Starting application");
        String sql = "SELECT name, room_number, bed_info FROM room";
        List<Room> rooms = new ArrayList<>();
        try {
            Connection connection = dataSource.getConnection();
            try(Statement stmt = connection.createStatement()) {
                ResultSet rs = stmt.executeQuery(sql);
                while (rs.next()) {
                    rooms.add(new Room(rs.getString("name"),
                            rs.getString("room_number"),
                            rs.getString("bed_info")));
                }
            }
        } catch (SQLException e) {
            LOG.error(e);
        }
        rooms.forEach(LOG::info);

        LOG.debug("Completing application");
        return 0;
    }
}
