package org.sromash.quarkus.example;

import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import jakarta.inject.Inject;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import java.util.List;

/**
 * Class Runner.
 * <p>
 * Date: 04 21, 2024
 *
 * @author Stanislau Romash
 */
@QuarkusMain
public class Runner implements QuarkusApplication {

    @Inject
    @RestClient
    RoomServiceRest roomService;

    @Override
    public int run(String... args) throws Exception {
        List<Room> rooms = roomService.getAllRooms();
        rooms.forEach(System.out::println);
        return 0;
    }
}
