package org.sromash.quarkus.example;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import java.util.List;

/**
 * Class RoomServiceRest.
 * <p>
 * Date: 04 21, 2024
 *
 * @author Stanislau Romash
 */
@Path("/rooms")
@ApplicationScoped
@RegisterRestClient(configKey = "rooms-api")
public interface RoomServiceRest {

    @GET
    List<Room> getAllRooms();
}
