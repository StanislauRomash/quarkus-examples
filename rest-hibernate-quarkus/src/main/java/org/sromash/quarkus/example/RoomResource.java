package org.sromash.quarkus.example;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.jboss.logging.Logger;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.nonNull;

/**
 * Class RoomResource.
 * <p>
 * Date: 04 21, 2024
 *
 * @author Stanislau Romash
 */
@Path("/rooms")
public class RoomResource {

    private static final Logger LOG = Logger.getLogger(RoomResource.class);
    RoomService roomService;
    MeterRegistry meterRegistry;

    @Inject
    public RoomResource(RoomService roomService,
                        MeterRegistry meterRegistry) {
        this.roomService = roomService;
        this.meterRegistry = meterRegistry;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Room> findRooms(@QueryParam("name") String name) {
        Timer timer = Timer.builder("roomservice").tag("method", "findRooms").register(meterRegistry);
        long start = System.nanoTime();
        LOG.info("findRooms is called");

        List<Room> rooms;
        if (nonNull(name) && !name.isBlank()) {
            rooms = roomService.findByName(name);
        } else {
            rooms = roomService.findAllRooms();
        }

        long end = System.nanoTime();
        timer.record(end - start, TimeUnit.MILLISECONDS);
        return rooms;
    }

    @GET
    @Path("/{roomNumber}")
    @Produces(MediaType.APPLICATION_JSON)
    public Room findByRoomNumber(@PathParam("roomNumber") String roomNumber) {
        LOG.info("findByRoomNumber is called");
        return roomService.findByRoomNumber(roomNumber);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Room save(@RequestBody RoomRequest roomRequest) {
        LOG.info("save is called");
        Room room = new Room(
                roomRequest.getName(),
                roomRequest.getRoomNumber(),
                roomRequest.getBedInfo()
        );
        roomService.save(room);
        return room;
    }
}
