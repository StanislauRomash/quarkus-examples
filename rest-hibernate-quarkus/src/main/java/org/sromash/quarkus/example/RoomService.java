package org.sromash.quarkus.example;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

import java.util.List;

/**
 * Class RoomService.
 * <p>
 * Date: 04 21, 2024
 *
 * @author Stanislau Romash
 */
@ApplicationScoped
public class RoomService {

    EntityManager entityManager;

    @Inject
    public RoomService(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Room> findAllRooms() {
        return entityManager
                .createQuery("select r from Room r", Room.class)
                .getResultList();
    }

    public List<Room> findByName(String name) {
        return entityManager
                .createQuery("select r from Room r where r.name = :name", Room.class)
                .setParameter("name", name)
                .getResultList();
    }

    public Room findByRoomNumber(String roomNumber) {
        return entityManager
                .createQuery("select r from Room r where r.roomNumber = :roomNumber", Room.class)
                .setParameter("roomNumber", roomNumber)
                .getSingleResult();
    }

    @Transactional
    public void save(Room room) {
        entityManager.persist(room);
    }
}
