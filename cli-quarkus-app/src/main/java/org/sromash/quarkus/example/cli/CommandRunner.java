package org.sromash.quarkus.example.cli;

import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import jakarta.inject.Inject;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

import java.util.Arrays;

import static java.util.Objects.nonNull;

/**
 * Class Main.
 * <p>
 * Date: 04 15, 2024
 *
 * @author Stanislau Romash
 */
@QuarkusMain
public class CommandRunner implements QuarkusApplication {
    private static final Logger LOG = Logger.getLogger(CommandRunner.class);

    @ConfigProperty(defaultValue = "30", name = "fizzBuzz.max")
    private int max;

    private FizzBuzz fizzBuzz;

    @Inject
    public CommandRunner(FizzBuzz fizzBuzz) {
        this.fizzBuzz = fizzBuzz;
    }

    @Override
    public int run(String... args) throws Exception {
        LOG.debug("App is started with max value: " + this.max);
        fizzBuzz.run(this.max);
        LOG.debug("App is finished");
        return 0;
    }
}
