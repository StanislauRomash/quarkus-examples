package org.sromash.quarkus.example.cli;

import jakarta.enterprise.context.ApplicationScoped;
import org.jboss.logging.Logger;

/**
 * Class FizzBuzz.
 * <p>
 * Date: 04 15, 2024
 *
 * @author Stanislau Romash
 */
@ApplicationScoped
public class FizzBuzz {

    private static final Logger LOG = Logger.getLogger(FizzBuzz.class);

    public static final int FIZZ = 3;
    public static final int BUZZ = 5;
    public static final int FIZZ_BUZZ = 15;

    public void run(int max) {
        LOG.info("FizzBuzz started for number: " + max);
        if (max < 1) {
            return;
        }
        for (int i = 1; i <= max; i++) {
            if (i % FIZZ_BUZZ == 0) {
                LOG.info(i + " fizz buzz");
            } else if (i % FIZZ == 0) {
                LOG.info(i + " fizz");
            } else if (i % BUZZ == 0) {
                LOG.info(i + " buzz");
            } else {
                LOG.info(i);
            }
        }
        LOG.info("FizzBuzz finished");
    }
}
