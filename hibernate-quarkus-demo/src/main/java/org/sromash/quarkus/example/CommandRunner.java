package org.sromash.quarkus.example;

import io.quarkus.arc.Arc;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import org.jboss.logging.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Class CommandRunner.
 * <p>
 * Date: 04 17, 2024
 *
 * @author Stanislau Romash
 */
@QuarkusMain
public class CommandRunner implements QuarkusApplication {

    private static final Logger LOG = Logger.getLogger(CommandRunner.class);

    @Inject
    EntityManager entityManager;

    @Override
    public int run(String... args) throws Exception {
        LOG.debug("Starting application");
        // is needed for Hibernates' sessions
        Arc.container().requestContext().activate();

        List<Room> rooms = entityManager
                .createQuery("SELECT r FROM Room r", Room.class)
                .getResultList();
        rooms.forEach(LOG::info);

        Arc.container().requestContext().deactivate();
        LOG.debug("Completing application");
        return 0;
    }
}
